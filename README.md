####  Deploy multicontainer Docker environment as an AWS Elastic Beanstalk application to AWS Cloud with Bitbucket Pipes


This repo contains a basic example for deploying  multicontainer Docker environment as an AWS Elastic Beanstalk application to AWS Cloud using [aws-elasticbeanstalk-deploy](https://bitbucket.org/atlassian/aws-elasticbeanstalk-deploy) pipe.
It this example we deploy a Sample Application [Multicontainer Docker](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/RelatedResources.html) as an AWS Elastic Beanstalk application.


#### Hot wo use thie example

##### Forking the repo
Create a fork of this repo, see [Forking a Repository](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html).


##### Prerequisites
* An IAM user is configured with sufficient permissions to perform a deployment to your application and upload artifacts to the S3 bucket.
* You have configured the Elastic Beanstalk application and environment.
* An S3 bucket has been set up to which deployment artifacts will be copied. Use name `${APPLICATION_NAME}-elasticbeanstalk-deployment}` to automatically use it.

Before update application you should create it in the AWS Elastic Beanstalk.

Follow the steps below to deploy this application to an Elastic Beanstalk Multi-container Docker environment. Accept the default settings unless indicated otherwise in the steps below:

1. Download the ZIP file from the AWS Elastic Beanstalk Resources [Multicontainer Docker](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/samples/docker-multicontainer-v2.zip) of this repository.
2. Login to the [Elastic Beanstalk Management Console](https://console.aws.amazon.com/elasticbeanstalk)
3. Click 'Create New Application' and give your app a name and description
4. Click 'Create web server'
5. Choose 'Multi-container Docker' in the 'Predefined configuration' dropdown and click `Next`
6. Upload the ZIP file downloaded in step 1
7. Review and launch the application
8. Deploy updates with [aws-elasticbeanstalk-deploy](https://bitbucket.org/atlassian/aws-elasticbeanstalk-deploy) pipe.


##### Enabling Pipelines in your repo 

To enable Piplines go to *Project*  > *Settings* > *Pipelines* and toggle the **Enable Pipelines** button. This can also be done from the **Pipelines** tab in your repository sidebar when you enable Pipelines for the first time. See also [Piplines Getting started](https://confluence.atlassian.com/bitbucket/get-started-with-bitbucket-pipelines-792298921.html) for more instructions.


##### Configuring pipe variables

To use this example you'll need to configure `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` pipelines variables in your repository.

See **User-defined variables** section in [Variables in pipelines](https://confluence.atlassian.com/bitbucket/variables-in-pipelines-794502608.html) for how to configure Pipelines variables. 


#### Verifying the results of the pipe run

After you've enabled Pipelines and configured all variables you can navigate to the Pipelines section of your fork and explore the current status of the builds. If the build is successful you should be able to navigate to `elasticbeanstalk.com URL` where your simple rating service should be up and running.
